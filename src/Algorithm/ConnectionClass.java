package Algorithm;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;

public class ConnectionClass {
	private static final Object NULL = null;
	Connection con = null;
	boolean valid = false;
	
	String sql ="";
	long card_number;
	int pin_code;
	String candidatefilename="";
	Blob candidateblob;
	String candidatefound="";
	public long length = 0 ;
	
	String oracleurl="jdbc:oracle:thin:@localhost:1521:xe";
    String oracleuname="hr";
    String oraclepwd="hr123";
    String oracledriver="oracle.jdbc.driver.OracleDriver";
    
    HashMap<String,String> map  = new HashMap<String, String>();
    
    CFingerPrint candidate_fingerprint = new CFingerPrint();
    BufferedImage candidate_bufferedimage = new BufferedImage(candidate_fingerprint.FP_IMAGE_WIDTH ,candidate_fingerprint.FP_IMAGE_HEIGHT,BufferedImage.TYPE_INT_RGB );
    double candidate_double[] = new double[candidate_fingerprint.FP_TEMPLATE_MAX_SIZE];
	
	public  ConnectionClass() { }
	
	public Connection createCon() {
		try {
			Class.forName(oracledriver);
			con = DriverManager.getConnection(oracleurl,oracleuname,oraclepwd); 
			return con;
		}
		catch (Exception e) {
		}
		return con;
	}	
	
	//validate card number
	public boolean checkcard(Connection con,String card) {
		this.con = con;
		card_number = Long.parseLong(card);
//		System.out.println(card_number);
		sql = "SELECT * from ACCOUNT_DATABASE where CUS_CARD_NO=?";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setLong(1,card_number);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				valid = true;
				
			}
			else {
				valid = false;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return valid;
	}
	
	//validate pin number with its card
	public boolean checkpin(Connection con,String pin) {
		this.con = con;
		pin_code = Integer.parseInt(pin);
		
		sql = "SELECT * from ACCOUNT_DATABASE where CUS_CARD_NO=? AND CUS_PIN_NO=?";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setLong(1, card_number);
			ps.setInt(2, pin_code);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) 
			{
				candidatefilename = rs.getString("CUS_FINGERPRINTLOCATION");
				
				try 
				{
					candidateblob = rs.getBlob("CUS_FINGERPRINTIMAGE");
					length = candidateblob.length();
					
					//if blob has image
					if((int)length>1) 
					{
						byte[] sec_image = candidateblob.getBytes(1,(int) length);
						InputStream in =  new ByteArrayInputStream(sec_image);
						candidate_bufferedimage=ImageIO.read(in) ;
						
						candidate_fingerprint.setFingerPrintImage(candidate_bufferedimage);
						candidate_fingerprint.ThinningHilditch();
						candidate_fingerprint.ThinningHitAndMiss();
									
						candidate_double = candidate_fingerprint.getFingerPrintTemplate();
						
						System.out.println("candidate has image : "+length);
						valid = true;
						
					}		
					
				}
				catch (Exception e) { 
					System.out.println(e);
				}

				valid = true;		
			}
			else	
			{
				valid = false;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return valid;
	}
	
	//validate fingerprint 
	public Double checking(double probe_double[]) {
		Double returnvalue;
		if(length<1)
		{
			returnvalue = 100.0;
		}
		else {
			if(probe_double.length == 0) {
				returnvalue = 0.0;
			}
			else {
				TestFingerPrint t = new TestFingerPrint();				
				returnvalue = t.twoImages(probe_double, candidate_double);
			}
		}
		return returnvalue;
	}
	
	// insert data into database
	public String insert(HashMap<String,String> map) {
		this.map = map;
		
		try {
			int acc_id = Integer.parseInt((String) map.get("f1"));
			String acc_type = (String) map.get("f2");
			int c_id = Integer.parseInt((String) map.get("f3"));
			int balance = Integer.parseInt((String) map.get("f4"));
			long card_no = Long.parseLong((String) map.get("f5"));
			int pin_no = Integer.parseInt((String) map.get("f6"));
			String fingerprint = (String) map.get("f7");
			
			File image = new File(fingerprint);
			FileInputStream fin1 =new FileInputStream(image);
			
			String sql = "insert into ATM_ACCOUNTS (ACC_ID, ACC_TYPE, C_ID, BALANCE, CARD_NUMBER, PIN_CODE, FINGERPRINTLOCATION, FINGERPRINT) values (?,?,?,?,?,?,?) ";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, acc_id);
			ps.setString(2, acc_type);
			ps.setInt(3, c_id);
			ps.setInt(4, balance);
			ps.setLong(5, card_no);
			ps.setInt(6, pin_no);
			ps.setString(7, fingerprint);
			ps.executeUpdate();	
			
			String trigger = "insert into ACCOUNT_DATABASE"
					+ "select cus.NAME,cus.CONTACT,b.BANK_NAME,acc.CARD_NUMBER,acc.PIN_CODE,acc.FINGERPRINTLOCATION,acc.FINGERPRINTIMAGE,acc.ACC_ID"
					+ "from ATM_CUSTOMERS cus JOIN ATM_BANKS b ON (cus.BANK_ID=b.BANK_ID)"
					+ "JOIN ATM_ACCOUNTS acc ON (acc.C_ID=cus.C_ID)"
					+ "where cus.C_ID = ?";
			PreparedStatement triggerps = con.prepareStatement(trigger);
			triggerps.setInt(1, c_id);
			triggerps.executeUpdate();
			
	
		}catch (Exception e) {
			return e.toString();
		}
		return "value inserted";
	}
	
	public String insert(HashMap<String,String> map,byte[] fingerprint_image) {
		this.map = map;
		
		try {
			int acc_id = Integer.parseInt((String) map.get("f1"));
			String acc_type = (String) map.get("f2");
			int c_id = Integer.parseInt((String) map.get("f3"));
			int balance = Integer.parseInt((String) map.get("f4"));
			long card_no = Long.parseLong((String) map.get("f5"));
			int pin_no = Integer.parseInt((String) map.get("f6"));
			String fingerprint = (String) map.get("f7");
			
			File image = new File(fingerprint);
			FileInputStream fin1 =new FileInputStream(image);
			
			String sql = "insert into ATM_ACCOUNTS (ACC_ID, ACC_TYPE, C_ID, BALANCE, CARD_NUMBER, PIN_CODE, FINGERPRINTLOCATION, FINGERPRINT) values (?,?,?,?,?,?,?,?) ";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, acc_id);
			ps.setString(2, acc_type);
			ps.setInt(3, c_id);
			ps.setInt(4, balance);
			ps.setLong(5, card_no);
			ps.setInt(6, pin_no);
			ps.setString(7, fingerprint);
			ps.setBytes(8, fingerprint_image);
			ps.executeUpdate();	
			
//			String trigger = "insert into ACCOUNT_DATABASE"
//					+ "select cus.NAME,cus.CONTACT,b.BANK_NAME,acc.CARD_NUMBER,acc.PIN_CODE,acc.FINGERPRINTLOCATION,acc.FINGERPRINTIMAGE,acc.ACC_ID"
//					+ "from ATM_CUSTOMERS cus JOIN ATM_BANKS b ON (cus.BANK_ID=b.BANK_ID)"
//					+ "JOIN ATM_ACCOUNTS acc ON (acc.C_ID=cus.C_ID)"
//					+ "where cus.C_ID = ?";
//			PreparedStatement triggerps = con.prepareStatement(trigger);
//			triggerps.setInt(1, c_id);
//			triggerps.executeUpdate();
			
	
		}catch (Exception e) {
			return e.toString();
		}
		return "value inserted";
	}
	
	
	public String update(HashMap<String,String> map,byte[] fingerprint_image) {
		this.map = map;	
		try {
			int acc_id = Integer.parseInt((String) map.get("f1"));
			String acc_type = (String) map.get("f2");
			int c_id = Integer.parseInt((String) map.get("f3"));
			int balance = Integer.parseInt((String) map.get("f4"));
			long card_no = Long.parseLong((String) map.get("f5"));
			int pin_no = Integer.parseInt((String) map.get("f6"));
			String fingerprint = (String) map.get("f7");
			
			File image = new File(fingerprint);
			FileInputStream fin1 =new FileInputStream(image);

//			String sql = "update ATM_ACCOUNTS SET FINGERPRINTLOCATION=?,FINGERPRINTIMAGE=? WHERE ACC_ID=?";
//			PreparedStatement ps = con.prepareStatement(sql);
//			ps.setString(1, fingerprint);
////			ps.setBinaryStream(2, fin1,(int) image.length());
//			ps.setBytes(2, fingerprint_image);
//			ps.setInt(3, c_id);
//			ps.executeUpdate();	
//			fin1.close();
			
			
			String sql = "update ATM_ACCOUNTS SET FINGERPRINTLOCATION=?,FINGERPRINTIMAGE=? WHERE ACC_ID=?";
			PreparedStatement ps = con.prepareStatement(sql);
			System.out.println("written "+fingerprint);
			ps.setString(1, fingerprint);
//			ps.setBinaryStream(2, fin1,(int) image.length());
			ps.setBytes(2, fingerprint_image);
			ps.setInt(3, acc_id);
			ps.executeUpdate();			
			
			
			String sql2 ="update ACCOUNT_DATABASE SET CUS_FINGERPRINTLOCATION=?,CUS_FINGERPRINTIMAGE=? WHERE ACC_ID=?";
			PreparedStatement ps2 = con.prepareStatement(sql2);
			ps2.setString(1, fingerprint);
//			ps2.setBinaryStream(2, fin1,(int) image.length());
			ps2.setBytes(2, fingerprint_image);
			ps2.setInt(3, acc_id);
			ps2.executeUpdate();	
			
			fin1.close();
	
		}catch (Exception e) {
			return e.toString();
		}
		return "value updated";
	
	}
	
	//update the database
	public String update(HashMap<String,String> map) {
		this.map = map;	
		try {
			int acc_id = Integer.parseInt((String) map.get("f1"));
			String acc_type = (String) map.get("f2");
			int c_id = Integer.parseInt((String) map.get("f3"));
			int balance = Integer.parseInt((String) map.get("f4"));
			long card_no = Long.parseLong((String) map.get("f5"));
			int pin_no = Integer.parseInt((String) map.get("f6"));
			String fingerprint = (String) map.get("f7");
			
			File image = new File(fingerprint);
			FileInputStream fin1 =new FileInputStream(image);

			String sql = "update ATM_ACCOUNTS SET FINGERPRINTLOCATION=?,FINGERPRINTIMAGE=? WHERE ACC_ID=?";
			PreparedStatement ps = con.prepareStatement(sql);
			System.out.println("written "+fingerprint);
			ps.setString(1, fingerprint);
			ps.setBinaryStream(2, fin1,(int) image.length());
			ps.setInt(3, acc_id);
			ps.executeUpdate();			
			
			
			String sql2 ="update ACCOUNT_DATABASE SET Cus_fingerprintlocation=?,Cus_fingerprintimage=? WHERE ACC_ID=?";
			PreparedStatement ps2 = con.prepareStatement(sql2);
			ps2.setString(1, fingerprint);
			ps2.setBinaryStream(2, fin1,(int) image.length());
			ps2.setInt(3, acc_id);
			ps2.executeUpdate();	
			
			fin1.close();
			
	
		}catch (Exception e) {
			return e.toString();
		}
		return "value updated";
	
	}
	
	public void close() {
		card_number=0;
		pin_code=0;
		try {
			con.close();
		} 
		catch (Exception e) {
		}
	}
	
}
