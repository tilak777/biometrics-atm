package Algorithm;

import java.awt.Image;
import java.util.*;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Utilities {

	Date d = new Date();
	String date="";
	String time="";
	
	public Utilities() { }
	
	// ImageIcon scaling 
	public Icon ImageIconToIcon(ImageIcon image,int width,int height) {
		Image img = image.getImage();
		Image newimg = img.getScaledInstance(width,height, Image.SCALE_SMOOTH);
		ImageIcon icon=new ImageIcon(newimg);
		return icon;		
	}
	
	// show system current time
	public String showTime(Date d) {
		int hour=d.getHours();
		int min =d.getMinutes();
		int sec = d.getSeconds();
		
		if(hour<12) {
			if(hour==0) {
				hour=12;
			}
			time =hour+":"+min+":"+sec+" AM";
		}
		else {
			if(hour==12)
			{
				time = hour+":"+min+":"+sec+" PM";
			}
			else
			{
			time =(hour-12)+":"+min+":"+sec+" PM";
			}
		}
		
		return time;
	}
	
	// show system current date 
	public String showDate(Date d) {
		@SuppressWarnings("deprecation")
		int getMonth = d.getMonth();
		@SuppressWarnings("deprecation")
		int getDate = d.getDate();
		@SuppressWarnings("deprecation")
		int getDay = d.getDay();
		
		String monthName="";
		String dayName="";
		
		switch(getMonth) {
		case 0:
			monthName="January";
			break;
		case 1:
			monthName="February";
			break;
		case 2:
			monthName="March";
			break;
		case 3:
			monthName="April";
			break;
		case 4:
			monthName="May";
			break;
		case 5:
			monthName="June";
			break;
		case 6:
			monthName="July";
			break;
		case 7:
			monthName="August";
			break;
		case 8:
			monthName="September";
			break;
		case 9:
			monthName="October";
			break;
		case 10:
			monthName="November";
			break;
		case 11:
			monthName="December";
			break;
		}
		
		switch(getDay) {
			case 0:
				dayName="Sun";
				break;
			case 1:
				dayName="Mon";
				break;
			case 2:
				dayName="Tues";
				break;
			case 3:
				dayName="Wed";
				break;
			case 4:
				dayName="Thurs";
				break;
			case 5:
				dayName="Fri";
				break;
			case 6:
				dayName="Sat";
				break;
		}
		date =dayName+"-"+monthName+"-"+getDate;
		return date;
	}
	
	
}
